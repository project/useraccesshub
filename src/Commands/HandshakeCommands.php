<?php

namespace Drupal\useraccesshub\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * Handshake drush commands.
 */
class HandshakeCommands extends DrushCommands {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct();
    $this->configFactory = $config_factory;
  }

  /**
   * Disable the handshake endpoint.
   *
   * @command useraccesshub:handshake_disable
   * @aliases uah:hd
   *
   * @usage useraccesshub:handshake_disable
   *   Disable the handshake endpoint on the site.
   */
  public function handshakeDisable() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('useraccesshub.settings');
    $config->set('handshake_enabled', FALSE);
    $config->save();

    $this->output()->writeLn('Handshake endpoint disabled successfully.');
  }

  /**
   * Enable the handshake endpoint.
   *
   * @command useraccesshub:handshake_enable
   * @aliases uah:hen
   *
   * @usage useraccesshub:handshake_enable
   *   Enable the handshake endpoint on the site.
   */
  public function handshakeEnable() {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->configFactory->getEditable('useraccesshub.settings');
    $config->set('handshake_enabled', TRUE);
    $config->save();

    $this->output()->writeLn('Handshake endpoint enabled successfully.');
  }

}
