<?php

namespace Drupal\useraccesshub\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form class.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'useraccesshub_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('useraccesshub.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable all of the User Access Hub functionality.'),
      '#default_value' => $config->get('enabled'),
    ];
    $form['redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL/Path'),
      '#description' => $this->t('Where to redirect the user after a login, including a preceding /. For example: /user.'),
      '#default_value' => $config->get('redirect'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config('useraccesshub.settings');
    $config->set('enabled', $values['enabled']);
    $config->set('redirect', $values['redirect']);
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['useraccesshub.settings'];
  }

}
