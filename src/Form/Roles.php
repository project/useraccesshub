<?php

namespace Drupal\useraccesshub\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Roles form class.
 */
class Roles extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'useraccesshub_roles_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('useraccesshub.settings');

    $role_entities = $this->entityTypeManager->getStorage('user_role')
      ->loadMultiple();
    $roles = [];
    /** @var \Drupal\user\Entity\Role $role */
    foreach ($role_entities as $role_id => $role) {
      // Remove authenticated and anonymous so they can't be selected.
      if ($role_id == RoleInterface::ANONYMOUS_ID || $role_id == RoleInterface::AUTHENTICATED_ID) {
        continue;
      }
      $roles[$role_id] = $role->label();
    }

    $form['role_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Roles'),
    ];
    $form['role_wrapper']['roles'] = [
      '#type' => 'checkboxes',
      '#title_display' => 'invisible',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Select which roles should have SSO login enabled from the User Access Hub. All roles not selected will still be able to login using the site login form.'),
      '#options' => $roles,
      '#default_value' => $config->get('roles'),
    ];
    $form['role_wrapper']['default_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Role'),
      '#description' => $this->t('Select which role should be considered the default.'),
      '#options' => $roles,
      '#default_value' => $config->get('default_role'),
    ];
    $form['allow_local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow local accounts to login (for managed roles above).'),
      '#default_value' => $config->get('allow_local'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $config = $this->config('useraccesshub.settings');
    $config->set('allow_local', $values['allow_local']);
    $config->set('roles', array_keys(array_filter($values['roles'])));
    $config->set('default_role', $values['default_role']);
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['useraccesshub.settings'];
  }

}
