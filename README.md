# User Access Hub

The [User Access Hub](https://www.useraccesshub.com/) is a service that allows administrators to manage users, user access, and roles across a network of CMS sites and will significantly simplify the administrative tasks associated with managing multiple websites.

[Create a free account to get started](https://www.useraccesshub.com/sign-up).

For a full description of the module, visit the [project page](https://www.drupal.org/project/useraccesshub).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/useraccesshub).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Overriding Configuration
- Drush Commands
- Maintainers
- Development

## Requirements

- This module requires the openssl_verify() PHP function, which is part of the [OpenSSL library](https://www.php.net/manual/en/book.openssl.php) for PHP.

- This module requires an account on [User Access Hub](https://www.useraccesshub.com/).

## Recommended modules

- [Markdown filter](https://www.drupal.org/project/markdown): When enabled, display of the project's README.md help will be rendered with markdown.
- [Config override warn](https://www.drupal.org/project/config_override_warn): When enabled, allows notification of overridden config values (in one of the settings.php files) in non-production environments.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend. This will create an API key at Configuration > User Access Hub > Authentication. None of the fields on this form can be edited through the UI. The 'Enable the Handshake Endpoint' checkbox should be checked.
1. Select the roles that should be handled by the hub's SSO functionality at Configuration > User Access Hub > Roles. Set a default role.
1. Add the site to the [User Access Hub](https://www.useraccesshub.com/) hub, setting the API key that was generated from step 1.
1. In the hub, use the 'Connect' operation to allow the hub to handshake with the site. Once this is complete, settings on Configuration > User Access Hub > Authentication will be updated - 'Public Key' will be populated, 'Site ID' will be populated and the 'Enable the Handshake Endpoint' checkbox will be unchecked.
1. To enable all User Access Hub functionality, the final step is to check the 'Enable all of the User Access Hub functionality.' checkbox on Configuration > User Access Hub > Settings.

The Drupal core roles of 'Anonymous' and 'Authenticated' are not available on the Configuration > User Access Hub > Roles form. Anonymous users do not need to login and all users that are created will also get the 'Authenticated' role. User Access Hub is meant to be used with defined site roles, however selecting the 'Default' role for a user in the hub will assign the default role to the site user when they login through the User Access Hub. 


## Overriding Configuration

In many cases, the configuration that exists as config entities will need to be overridden. The case of different config values for different environments springs to mind. This can be accomplished with the [Config_split](https://www.drupal.org/project/config_split) module or by overriding the configuration in a settings.php file:

```php
$config['useraccesshub.settings']['api_key'] = 'string';
$config['useraccesshub.settings']['allow_local'] = FALSE;
$config['useraccesshub.settings']['enabled'] = FALSE;
$config['useraccesshub.settings']['handshake_enabled'] = TRUE;
$config['useraccesshub.settings']['public_key'] = 'string';
$config['useraccesshub.settings']['roles'] = ['role1', 'role2'];
$config['useraccesshub.settings']['site_id'] = 1;
$config['useraccesshub.settings']['redirect'] = '/redirect/path';
$config['useraccesshub.settings']['default_role'] = 'role2';
```

## Drush Commands

There are two drush commands that allow for enabling and disabling the handshake endpoint, for situations where a site needs to be re-connected. This can also be accomplished by overriding the configuration (see above).

Enable the handshake endpoint:

```bash
drush uah:hen
```

Disable the handshake endpoint:

```bash
drush uah:hd
```

## Maintainers

- Scott Joudry - [slydevil](https://www.drupal.org/u/slydevil)

## Development

Make sure the dev tools are available:

```bash
composer install
```

Check the code using phpcs from the module directory:

```bash
./vendor/bin/phpcs -p --ignore=vendor
```

Check the code using phpstan from the site root (requires a Drupal site with module installed):

```bash
./web/modules/contrib/useraccesshub/vendor/bin/phpstan analyse -c ./web/modules/contrib/useraccesshub/phpstan.neon
```

Modify phpstan and phpcs configuration in phpstan.neon and phpcs.xml.dist respectively.
