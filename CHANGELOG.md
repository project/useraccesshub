# Changelog

## User Access Hub 1.0.11, 2024-XX-XX

- Added documentation for overriding configuration.

## User Access Hub 1.0.10, 2024-04-18

- Added phpstan and config file, fixed some phpcs issues.
- Added two drush commands to enable and disable the handshake endpoint.

## User Access Hub 1.0.10, 2024-04-07

- Added /spoke/api/packages endpoint to allow the hub to pull core/module/theme data.
- Issue #3435299 - Automated Drupal 11 compatibility fixes for useraccesshub.

## User Access Hub 1.0.9, 2024-03-21

- Issue #3432810 - Cannot de-select roles
- Issue #3432873 - Default role is set to 'authenticated' and should not be set during installation.

## User Access Hub 1.0.8, 2024-03-07

- Issue #3426216 - Prevent locked roles from being selected as default

## User Access Hub 1.0.7, 2024-01-29

- Issue #3417916 - Unable to login after update.

## User Access Hub 1.0.6, 2024-01-29

- Issue #3417916 - Unable to login after update.

## User Access Hub 1.0.5, 2024-01-25

- Issue #3415643 - Roles endpoint should accept POST rather than GET
- Issue #3415644 - Add time validation on roles endpoint

## User Access Hub 1.0.4, 2023-12-03

- Added a default role configuration option.
- Added a redirect URL configuration option.
- Minor documentation fixes.

## User Access Hub 1.0.3, 2023-12-02

- Routes had wrong permissions.

## User Access Hub 1.0.2, 2023-12-02

- Re-added support for Drupal 9.

## User Access Hub 1.0.1, 2023-11-28

- Useracceshub UI sends multiple roles, updated the module to accept them.

## User Access Hub 1.0.0, 2023-09-19

- Initial port of the Drupal Admin module (https://www.drupal.org/project/drupal_admin)
